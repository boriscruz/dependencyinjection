package com.springboot.di.app.models.services;

public class MyService implements IService {

	@Override
	public String operation() {
		return "Executing any important process";
	}

}
