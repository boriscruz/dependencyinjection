package com.springboot.di.app.models.services;

public class MyComplexService implements IService {

	@Override
	public String operation() {
		return "Executing any important complex process";
	}

}
