package com.springboot.di.app.models.domain;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
@RequestScope
public class Invoice {

	@Value("${invoice.description}")
	private String description;

	@Autowired
	private Customer customer;

	@Autowired
	private List<ItemInvoice> items;

	@PostConstruct
	public void init() {
		customer.setName(customer.getName().concat(" Belén"));
		description = description.concat(": " + customer.getName());
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("Invoice Destroyed. ".concat(description));
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<ItemInvoice> getItems() {
		return items;
	}

	public void setItems(List<ItemInvoice> items) {
		this.items = items;
	}

}
