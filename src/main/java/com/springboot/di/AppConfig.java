package com.springboot.di;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.springboot.di.app.models.domain.ItemInvoice;
import com.springboot.di.app.models.domain.Product;
import com.springboot.di.app.models.services.IService;
import com.springboot.di.app.models.services.MyComplexService;
import com.springboot.di.app.models.services.MyService;

@Configuration
@PropertySources({
    @PropertySource(value = "classpath:text.properties", encoding = "UTF-8"),
    @PropertySource(value = "classpath:application.properties", encoding = "UTF-8")}
)
public class AppConfig {

	@Bean("MySimpleService")
	public IService registerMyService() {
		return new MyService();
	}

	@Bean("MyComplexService")
	public IService registerMyComplexService() {
		return new MyComplexService();
	}

	@Bean("itemsInvoice")
	public List<ItemInvoice> registerItems() {
		Product product1 = new Product("Shirt", 3200);
		Product product2 = new Product("Pants", 2100);

		ItemInvoice line1 = new ItemInvoice(product1, 2);
		ItemInvoice line2 = new ItemInvoice(product2, 3);

		return Arrays.asList(line1, line2);

	}

}
